#!/bin/bash

pod() {
mkdir -p /root/.ssh
touch /root/.ssh/id_rsa
echo "$SSH_KEYS" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa

node_name=$(ssh -o StrictHostKeyChecking=no $user@$ip -p $port -i /root/.ssh/id_rsa kubectl get nodes --no-headers | grep -v master | awk 'FNR==1 {print $1}')
echo $node_name
ssh -o StrictHostKeyChecking=no $user@$ip -p $port -i /root/.ssh/id_rsa 'cd e2e-konvoy && bash openebs-konvoy-e2e/pipelines/OpenEBS-base/stages/5-infra-chaos/5JI03-node-failure-jiva/node-failure-jiva node '"'$CI_JOB_ID'"'' '"'$CI_PIPELINE_ID'"' '"'$CI_COMMIT_SHA'"' '"'$node_name'"' '"'$RELEASE_TAG'"'
}

node() {
job_id=$(echo $1)
pipeline_id=$(echo $2)
commit_id=$(echo $3)
source ~/.profile
gittoken=$(echo "$github_token")
case_id=5JI03
observer_node=$4

time="date"
current_time=$(eval $time)

present_dir=$(pwd)
echo $present_dir
bash openebs-konvoy-e2e/utils/pooling jobname:kubelet-failure-jiva
bash openebs-konvoy-e2e/utils/e2e-cr jobname:node-failure-jiva jobphase:Running init_time:"$current_time" jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id" openebs_version:"$releaseTag"

################
# e2eBook 1 #
################

echo "*******Deploying Busybox-Deployment****"

test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=busybox-provision-node-failure-jiva metadata="")
echo $test_name

cd e2e-tests
echo "Running the e2e test for Busybox Deployment.."
cp apps/busybox/deployers/run_e2e_test.yml app_deploy_node_failure_jiva.yml

: << EOF
  -------------------------------------------------------------------------------------------------------------------
 | specAttribute     | kind   |         default value               | test specifc value                             |
  -------------------------------------------------------------------------------------------------------------------|
 | appNamespace      | env    | app-busybox-ns                      | node-failure-jiva                              |
 | e2e job label  | label  | app: busybox-e2e                 | app: node-failure-jiva                         | 
 | appLabel          | env    | app=busybox-sts                     | app=node-failure-jiva                          |
 | storage class     | env    | openebs-cstor-sparse                | openebs-jiva-defaul                            |
 | deploy type       | env    | statefulset                         | deployment                                     |
  --------------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/app-busybox-ns/node-failure-jiva/g' \
-e 's/busybox-e2e/node-failure-jiva/g' \
-e 's/app=busybox-sts/app=node-failure-jiva/g' \
-e 's/openebs-cstor-sparse/openebs-jiva-default/g' \
-e 's/statefulset/deployment/g' app_deploy_node_failure_jiva.yml

cat app_deploy_node_failure_jiva.yml

bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='app:node-failure-jiva' app_deploy_node_failure_jiva.yml
cd ..
bash openebs-konvoy-e2e/utils/dump_cluster_state;
bash openebs-konvoy-e2e/utils/event_updater jobname:node-failure-jiva $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

if [ "$?" != "0" ]; then
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id 5JI03 5-infra-chaos "Fail the node where the application is running and check the behaviour" Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

################
# e2eBook 2 #
################

run_id="node-failure-jiva";test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=busybox-liveness metadata=${run_id})
echo $test_name

cd e2e-tests
# copy the content of deployer run_e2e_test.yml into a different file to update the test specific parameters.
cp apps/busybox/liveness/run_e2e_test.yml busybox_loadgen_node_failure_jiva.yml

# Update the environmental variables in e2e job spec.

: << EOF
  ----------------------------------------------------------------------------------------------------------------------
 | specAttribute     | kind   |         default value               | test specifc value                               |
  ---------------------------------------------------------------------------------------------------------------------|
 | appNamespace      | env    | app-busybox-ns                      | node-failure-jiva                                       | 
 | e2e job label  | label  | liveness: e2e-busybox-liveness   | liveness: busybox-liveness-node-failure-jiva             |
 | appLabel          | env    | app=busybox-sts                     | app=node-failure-jiva                                   | 
  ----------------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/value: app-busybox-ns/value: node-failure-jiva/g' \
-e 's/app=busybox-sts/app=node-failure-jiva/g' \
-e 's/liveness: e2e-busybox-liveness/liveness: busybox-liveness-node-failure-jiva/g' busybox_loadgen_node_failure_jiva.yml

cat busybox_loadgen_node_failure_jiva.yml

sed -i '/command:/i \
          - name: RUN_ID\
            value: '"$run_id"'\
' busybox_loadgen_node_failure_jiva.yml

# Run the e2e job and get the details of the e2e job from e2e_job_runner utils.
bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='liveness:busybox-liveness-node-failure-jiva' job=busybox_loadgen_node_failure_jiva.yml
cd ..
# Get the cluster state Once the e2e jobs completed.
bash openebs-konvoy-e2e/utils/dump_cluster_state;
# Update the e2e event for the job.
bash openebs-konvoy-e2e/utils/event_updater jobname:node-failure-jiva $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

if [ "$?" != "0" ]; then
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id 5JI03 5-infra-chaos "Fail the node where the application is running and check the behaviour" Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

################
# e2eBook 3 #
################

run_id="jiva";test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=node-failure metadata=${run_id})
echo $test_name

cd e2e-tests
cp experiments/chaos/node_failure/run_e2e_test.yml run_node_failure_jiva.yml

sed -i -e 's/value: app-percona-ns/value: node-failure-jiva/g' \
-e 's/password:/password: cGFzc3dvcmQ=/g' \
-e 's/name=percona/app=node-failure-jiva/g' run_node_failure_jiva.yml

sed -i '/name: APP_PVC/{n;s/.*/            value: openebs-busybox/}' run_node_failure_jiva.yml

sed -i -e "s|#nodeSelector|nodeSelector|g" \
-e "s|name: node-failure|name: node-failure-jiva|g" \
-e "s|name: host-password|name: host-password-jiva|g" \
-e "s|#  kubernetes.io/hostname:|  kubernetes.io/hostname: ${observer_node}|g" run_node_failure_jiva.yml

sed -i '/name: DATA_PERSISTENCE/{n;s/.*/            value: busybox/}' run_node_failure_jiva.yml

sed -i '/name: ESX_HOST_IP/{n;s/.*/            value: 10.33.1.1/}' run_node_failure_jiva.yml

sed -i '/parameters.yml: |/a \
    blocksize: 4k\
    blockcount: 1024\
    testfile: nodefailurejiva
' run_node_failure_jiva.yml

sed -i '/command:/i \
          - name: RUN_ID\
            value: '"$run_id"'\
' run_node_failure_jiva.yml

cat run_node_failure_jiva.yml

bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='name:node-failure-jiva' run_node_failure_jiva.yml
cd ..
bash openebs-konvoy-e2e/utils/dump_cluster_state;
bash openebs-konvoy-e2e/utils/event_updater jobname:node-failure-jiva $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

if [ "$?" != "0" ]; then
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id 5JI03 5-infra-chaos "Fail the node where the application is running and check the behaviour" Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

################
# e2eBook 4 #
################

run_id="deprovision-node-failure-jiva";test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=busybox-liveness metadata=${run_id})
echo $test_name

cd e2e-tests
# copy the content of deployer run_e2e_test.yml into a different file to update the test specific parameters.
cp apps/busybox/liveness/run_e2e_test.yml busybox_loadgen_deprovision_node_failure_jiva.yml

# Update the environmental variables in e2e job spec.

: << EOF
  ---------------------------------------------------------------------------------------------------------------------
 | specAttribute     | kind   |         default value               | test specifc value                               |
  ---------------------------------------------------------------------------------------------------------------------|
 | appNamespace      | env    | app-busybox-ns                      | node-failure-jiva                                | 
 | appLabel          | env    | app=busybox-sts                     | app=node-failure-jiva                            |
 | e2e job label  | label  | liveness: e2e-busybox-liveness   | liveness: busybox-liveness-deprovision-node-failure|
 | action            | env    | provision                           | deprovision                                      | 
  ----------------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/value: app-busybox-ns/value: node-failure-jiva/g' \
-e 's/app=busybox-sts/app=node-failure-jiva/g' \
-e 's/value: provision/value: deprovision/g' \
-e 's/generateName: e2e-busybox-liveness/generateName: busybox-liveness-dep-node-failure-jiva/g' \
-e 's/liveness: e2e-busybox-liveness/liveness: busybox-liveness-deprovision-node-failure-jiva/g' busybox_loadgen_deprovision_node_failure_jiva.yml

sed -i '/command:/i \
          - name: RUN_ID\
            value: '"$run_id"'\
' busybox_loadgen_deprovision_node_failure_jiva.yml

cat busybox_loadgen_deprovision_node_failure_jiva.yml
# Run the e2e job and get the details of the e2e job from e2e_job_runner utils.
bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='liveness:busybox-liveness-deprovision-node-failure-jiva' job=busybox_loadgen_deprovision_node_failure_jiva.yml
cd ..
# Get the cluster state Once the e2e jobs completed.
bash openebs-konvoy-e2e/utils/dump_cluster_state;
bash openebs-konvoy-e2e/utils/event_updater jobname:node-failure-jiva $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

rc_val=$(echo $?)

# Update result of the test case in github mayadata-io/e2e-konvoy repository.
if [ "$rc_val" != "0" ]; then
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id 5JI03 5-infra-chaos "Fail the node where the application is running and check the behaviour" Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

################
# e2eBook 5 #
################

echo "********Deprovisioning Busybox Application*******"

test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=busybox-deprovision-node-failure-jiva metadata="")
echo $test_name

cd e2e-tests
cp apps/busybox/deployers/run_e2e_test.yml deprovision_node_failure_jiva.yml

: << EOF
  -------------------------------------------------------------------------------------------------------------------
 | specAttribute     | kind   |         default value               | test specifc value                             |
  -------------------------------------------------------------------------------------------------------------------|
 | e2e Job name   | name   | generateName: e2e-busybox-deploy | generateName:e2e-busybox-deprovision        |
 | appNamespace      | env    | app-busybox-ns                      | node-failure-jiva                              |
 | appLabel          | env    | app=busybox-sts                     | app=node-failure-jiva                          |
 | deploy type       | env    | statefulset                         | deployment                                     |
 | e2e job label  | label  | app: busybox-e2e                 | app: busybox-deprovision-node-failure          | 
 | storage class     | env    | openebs-cstor-sparse                | openebs-jiva-default                           |
 | Action            | env    | provision                           | deprovision                                    |
  --------------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/generateName: e2e-busybox-deploy/generateName: e2e-busybox-dep-node-failure-jiva/g' \
-e 's/app-busybox-ns/node-failure-jiva/g' \
-e 's/app=busybox-sts/app=node-failure-jiva/g' \
-e 's/value: statefulset/value: deployment/g' \
-e 's/busybox-e2e/busybox-deprovision-node-failure-jiva/g' \
-e 's/openebs-cstor-sparse/openebs-jiva-default/g' \
-e 's/value: provision/value: deprovision/g' deprovision_node_failure_jiva.yml

cat deprovision_node_failure_jiva.yml

bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='app:busybox-deprovision-node-failure-jiva' deprovision_node_failure_jiva.yml
cd ..
bash openebs-konvoy-e2e/utils/dump_cluster_state;
bash openebs-konvoy-e2e/utils/event_updater jobname:node-failure-jiva $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

rc_val=$(echo $?)
current_time=$(eval $time)

# testResult=$(kubectl get e2eresult ${test_name} --no-headers -o custom-columns=:spec.testStatus.result)

if [ "$rc_val" != "0" ]; then
bash openebs-konvoy-e2e/utils/e2e-cr jobname:node-failure-jiva jobphase:Completed end_time:"$current_time" jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id" openebs_version:"$releaseTag" test_result:Fail
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id 5JI03 5-infra-chaos "Fail the node where the application is running and check the behaviour" Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

bash openebs-konvoy-e2e/utils/e2e-cr jobname:node-failure-jiva jobphase:Completed end_time:"$current_time" jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id" openebs_version:"$releaseTag" test_result:Pass
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id 5JI03 5-infra-chaos "Fail the node where the application is running and check the behaviour" Pass $pipeline_id "$current_time" $commit_id $gittoken

if [ "$rc_val" != "0" ]; then
exit 1;
fi
}

if [ "$1" == "node" ];then
  node $2 $3 $4 $5 $6
else
  pod
fi
