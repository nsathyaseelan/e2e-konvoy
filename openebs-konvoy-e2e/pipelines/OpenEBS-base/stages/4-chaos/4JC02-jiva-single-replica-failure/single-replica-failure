#!/bin/bash


pod() {
mkdir -p /root/.ssh
touch /root/.ssh/id_rsa
echo "$SSH_KEYS" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
ssh -o StrictHostKeyChecking=no $user@$ip -p $port -i /root/.ssh/id_rsa 'cd e2e-konvoy && bash openebs-konvoy-e2e/pipelines/OpenEBS-base/stages/4-chaos/4JC02-jiva-single-replica-failure/single-replica-failure node '"'$CI_JOB_ID'"'' '"'$CI_PIPELINE_ID'"' '"'$CI_COMMIT_SHA'"' '"'$RELEASE_TAG'"'
}

node() {
source ~/.profile
#github token to push the test result into github openebs/e2e-openshift repository. 
#This token is set as an env in ~/.profile in the test cluster.
gittoken=$(echo "$github_token")

job_id=$(echo $1) #Gitlab job id obtain from the gitlab env ($CI_JOB_ID).
pipeline_id=$(echo $2) # Gitlab pipeline id obtained from gitlab env ($CI_PIPELINE_ID).
commit_id=$(echo $3) #Gitlab commit id Obtained fron gilab env ($CI_COMMIT_SHA).
releaseTag=$(echo $4)
case_id=4JC02 #test case ID.
gitlab_stage=4-chaos # Gitlab stage where this test case is running.
#job_description="'Induce failure on JIVA single replica deployment and check if it gets scheduled immediately and the application is available'"

time="date"
current_time=$(eval $time)

present_dir=$(pwd)
echo $present_dir

#Creating e2e custom resoure result fot the each stage of the test.

bash openebs-konvoy-e2e/utils/e2e-cr jobname:jiva-single-rep-kill jobphase:Waiting init_time:"$current_time" jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id" openebs_version:"$releaseTag"
bash openebs-konvoy-e2e/utils/e2e-cr jobname:jiva-single-rep-kill jobphase:Running init_time:"$current_time"

################
# e2eBook 1 #
################
#### Deploy BusyBox application

#testcase name for the busybox deployers will be append with busybox-{{ action }}- {{ app-namespace }}
#action will be provision or deprovision.
#app namespaces is the name to deploy the busybox application.
test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=busybox-provision-single-rep-kill metadata="")
echo $test_name

cd e2e-tests
echo "Running the e2e test for Busybox Deployment.."

#copy the content of deployer run_e2e_test.yml into a different file to update the test specific parametes.
cp apps/busybox/deployers/run_e2e_test.yml busybox_single_rep_kill.yml

: << EOF
  --------------------------------------------------------------------------------------------------------------
 | specAttribute     | kind     | default busybox value               | test specifc value                      |
  --------------------------------------------------------------------------------------------------------------|
 | e2e job label  | label    | app: busybox-e2e                 | app: deploy-busybox-single-rep          |
 | e2e job Name   | job name | generateName: e2e-busybox-deploy | generateName: busybox-deploy-single-rep |
 | appLabel          | env      | app=busybox-sts                     | app=single-rep-kill                     |
 | deploy type       | env      | statefulset                         | deployment                              |
 | storage class     | env      | openebs-cstor-sparse                | openebs-jiva-standalone                 |
 | pvcName           | env      | openebs-busybox                     | busybox-single-rep-kill                 | 
 | appNamespace      | env      | app-busybox-ns                      | single-rep-kill	                        | 
  --------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/app: busybox-e2e/app: deploy-busybox-single-rep/g' \
-e 's/generateName: e2e-busybox-deploy/generateName: busybox-deploy-single-rep/g' \
-e 's/app=busybox-sts/app=single-rep-kill/g' \
-e 's/value: statefulset/value: deployment/g' \
-e 's/value: openebs-cstor-sparse/value: openebs-jiva-standalone/g' \
-e 's/value: openebs-busybox/value: busybox-single-rep-kill/g' \
-e 's/value: app-busybox-ns/value: single-rep-kill/g' busybox_single_rep_kill.yml

cat busybox_single_rep_kill.yml

# Run the e2e job and get the details of the e2e job from e2e_job_runner utils.
bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='app:deploy-busybox-single-rep' job=busybox_single_rep_kill.yml
cd ..

#Get the cluster state Once the e2e jobs completed.
bash openebs-konvoy-e2e/utils/dump_cluster_state;
#Update the e2e event for the job
bash openebs-konvoy-e2e/utils/event_updater jobname:jiva-single-rep-kill $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"
#Update the result of the test case in github openebs/e2e-openshift repository.
if [ "$?" != "0" ]; then
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id $case_id $gitlab_stage 'Induce failure on JIVA single replica deployment and check if it gets scheduled immediately and the application is available' Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

################
# e2eBook 2 #
################

run_id="single-rep-kill";test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=busybox-liveness metadata=${run_id})
echo $test_name

cd e2e-tests
# copy the content of deployer run_e2e_test.yml into a different file to update the test specific parameters.
cp apps/busybox/liveness/run_e2e_test.yml busybox_loadgen_single_rep_kill.yml

# Update the environmental variables in e2e job spec.

: << EOF
  ---------------------------------------------------------------------------------------------------------------------
 | specAttribute     | kind   |         default value               | test specifc value                               |
  ---------------------------------------------------------------------------------------------------------------------|
 | appNamespace      | env    | app-busybox-ns                      | single-rep-kill                                  | 
 | e2e job label  | label  | liveness: e2e-busybox-liveness   | liveness: busybox-liveness-single-rep-kill       |
 | appLabel          | env    | app=busybox-sts                     | app=single-rep-kill                              | 
  ----------------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/value: app-busybox-ns/value: single-rep-kill/g' \
-e 's/app=busybox-sts/app=single-rep-kill/g' \
-e 's/liveness: e2e-busybox-liveness/liveness: busybox-liveness-single-rep-kill/g' busybox_loadgen_single_rep_kill.yml

cat busybox_loadgen_single_rep_kill.yml

sed -i '/command:/i \
          - name: RUN_ID\
            value: '"$run_id"'\
' busybox_loadgen_single_rep_kill.yml

# Run the e2e job and get the details of the e2e job from e2e_job_runner utils.
bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='liveness:busybox-liveness-single-rep-kill' job=busybox_loadgen_single_rep_kill.yml
cd ..
# Get the cluster state Once the e2e jobs completed.
bash openebs-konvoy-e2e/utils/dump_cluster_state;
# Update the e2e event for the job.
bash openebs-konvoy-e2e/utils/event_updater jobname:jiva-single-rep-kill $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

if [ "$?" != "0" ]; then
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id 4JC02 4-chaos "Induce failure on JIVA single replica deployment and check if it gets scheduled immediately and the application is available" Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

################
# e2eBook 3 #
################
##### Run the e2e book to perform the Single Replica Failure failure on JIVA

## testcase name is defined in test_vars of e2eBook.
run_id="single";test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=openebs-volume-replica-failure metadata=${run_id})
echo $test_name

cd e2e-tests
cp experiments/chaos/openebs_volume_replica_failure/run_e2e_test.yml run_single_rep_failure.yml

: << EOF
  --------------------------------------------------------------------------------------------------------------------
 | specAttribute   | kind |         default value                         | test specifc value                       |
  -------------------------------------------------------------------------------------------------------------------|
 | pvcName         | env  | value: percona-mysql-claim                    | value: busybox-single-rep-kill           | 
 | e2e Job name | name | generateName: openebs-volume-replica-failure  | generateName: single-replica-failure     |
 | e2e job label| label| openebs-volume-replica-failure                | single-replica-failure                   |
 | appLabel        | env  | value: name=percona                           | value:  app=single-rep-kill              |
 | appNamespace    | env  | app-percon-ns                                 | single-rep-kill                          | 
  --------------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/value: percona-mysql-claim/value: busybox-single-rep-kill/g' \
-e 's/generateName: openebs-volume-replica-failure/generateName: single-replica-failure/g' \
-e 's/openebs-volume-replica-failure/single-replica-failure/g' \
-e 's/value: '\''name=percona'\''/value: '\''app=single-rep-kill'\''/g' \
-e 's/value: app-percona-ns/value: single-rep-kill/g' run_single_rep_failure.yml

sed -i '/command:/i \
          - name: RUN_ID\
            value: '"$run_id"'\
' run_single_rep_failure.yml

cat run_single_rep_failure.yml

bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='name:single-replica-failure' job=run_single_rep_failure.yml
cd ..
bash openebs-konvoy-e2e/utils/dump_cluster_state;
bash openebs-konvoy-e2e/utils/event_updater jobname:jiva-single-rep-kill $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

if [ "$?" != "0" ]; then
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id $case_id $gitlab_stage 'Induce failure on JIVA single replica deployment and check if it gets scheduled immediately and the application is available' Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

################
# e2eBook 4 #
################

run_id="deprovision-single-rep-kill";test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=busybox-liveness metadata=${run_id})
echo $test_name

cd e2e-tests
# copy the content of deployer run_e2e_test.yml into a different file to update the test specific parameters.
cp apps/busybox/liveness/run_e2e_test.yml busybox_loadgen_deprovision_single_rep_kill.yml

# Update the environmental variables in e2e job spec.

: << EOF
  ----------------------------------------------------------------------------------------------------------------------------
 | specAttribute     | kind   |         default value               | test specifc value                                     |
  ---------------------------------------------------------------------------------------------------------------------------|
 | appNamespace      | env    | app-busybox-ns                      | single-rep-kill                                        | 
 | appLabel          | env    | app=busybox-sts                     | app=single-rep-kill                                    |
 | e2e job label  | label  | liveness: e2e-busybox-liveness   | liveness: busybox-liveness-deprovision-single-rep-kill |
 | action            | env    | provision                           | deprovision                                            | 
  ----------------------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/value: app-busybox-ns/value: single-rep-kill/g' \
-e 's/app=busybox-sts/app=single-rep-kill/g' \
-e 's/value: provision/value: deprovision/g' \
-e 's/liveness: e2e-busybox-liveness/liveness: busybox-liveness-deprovision-single-rep-kill/g' busybox_loadgen_deprovision_single_rep_kill.yml

sed -i '/command:/i \
          - name: RUN_ID\
            value: '"$run_id"'\
' busybox_loadgen_deprovision_single_rep_kill.yml

cat busybox_loadgen_deprovision_single_rep_kill.yml
# Run the e2e job and get the details of the e2e job from e2e_job_runner utils.
bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='liveness:busybox-liveness-deprovision-single-rep-kill' job=busybox_loadgen_deprovision_single_rep_kill.yml
cd ..
# Get the cluster state Once the e2e jobs completed.
bash openebs-konvoy-e2e/utils/dump_cluster_state;
bash openebs-konvoy-e2e/utils/event_updater jobname:jiva-single-rep-kill $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

rc_val=$(echo $?)

# Update result of the test case in github mayadata-io/e2e-openshift repository.
if [ "$rc_val" != "0" ]; then
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id 4JC02 4-chaos "Induce failure on JIVA single replica deployment and check if it gets scheduled immediately and the application is available" Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

################
# e2eBook 5 #
################
# Run the e2e book to Deprovison the busybox deployment

echo "********Deprovisioning Busybox Application*******"

test_name=$(bash openebs-konvoy-e2e/utils/generate_test_name testcase=busybox-deprovision-single-rep-kill metadata="")
echo $test_name

cd e2e-tests
cp apps/busybox/deployers/run_e2e_test.yml deprovision_single_rep_kill.yml

: << EOF
  ----------------------------------------------------------------------------------------------------------------------
 | specAttribute     | kind   |         default value               | test specifc value                               |
  ---------------------------------------------------------------------------------------------------------------------|
 | e2e Job name   | name   | generateName: e2e-busybox-deploy | generateName: busybox-deprovision-single-rep-kill|
 | e2e job label  | label  | app: busybox-e2e                 | app: busybox-deprovision-single-rep-kill         |
 | appLabel          | env    | app=busybox-sts                     | app=single-rep-kill                              |
 | deploy type       | env    | statefulset                         | deployment                                       |
 | storage clas      | env    | openebs-cstor-sparse                | openebs-jiva-standalone                          |
 | pvcName           | env    | openebs-busybox                     | busybox-single-rep-kill                          | 
 | appNamespace      | env    | app-busybox-ns                      | single-rep-kill                                  | 
 | Action            | env    | value: provision                    | value: deprovision                               |
  ----------------------------------------------------------------------------------------------------------------------
EOF

sed -i -e 's/generateName: e2e-busybox-deploy/generateName: busybox-deprovision-single-rep-kill/g' \
-e 's/app: busybox-e2e/app: busybox-deprovision-single-rep-kill/g' \
-e 's/app=busybox-sts/app=single-rep-kill/g' \
-e 's/value: statefulset/value: deployment/g' \
-e 's/value: openebs-cstor-sparse/value: openebs-jiva-standalone/g' \
-e 's/value: openebs-busybox/value: busybox-single-rep-kill/g' \
-e 's/value: app-busybox-ns/value: single-rep-kill/g' \
-e 's/value: provision/value: deprovision/g' deprovision_single_rep_kill.yml

cat deprovision_single_rep_kill.yml

bash ../openebs-konvoy-e2e/utils/e2e_job_runner label='app:busybox-deprovision-single-rep-kill' job=deprovision_single_rep_kill.yml
cd ..
bash openebs-konvoy-e2e/utils/dump_cluster_state;
bash openebs-konvoy-e2e/utils/event_updater jobname:jiva-single-rep-kill $test_name jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id"

rc_val=$(echo $?)
current_time=$(eval $time)
# testResult=$(kubectl get e2eresult ${test_name} --no-headers -o custom-columns=:spec.testStatus.result)

if [ "$rc_val" != "0" ]; then
bash openebs-konvoy-e2e/utils/e2e-cr jobname:jiva-single-rep-kill jobphase:Completed end_time:"$current_time" jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id" openebs_version:"$releaseTag" test_result:Fail
python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id $case_id $gitlab_stage 'Induce failure on JIVA single replica deployment and check if it gets scheduled immediately and the application is available' Fail $pipeline_id "$current_time" $commit_id $gittoken
exit 1;
fi

bash openebs-konvoy-e2e/utils/e2e-cr jobname:jiva-single-rep-kill jobphase:Completed end_time:"$current_time" jobid:"$job_id" pipelineid:"$pipeline_id" testcaseid:"$case_id" openebs_version:"$releaseTag" test_result:Pass

python3 openebs-konvoy-e2e/utils/result/result_update.py $job_id $case_id $gitlab_stage 'Induce failure on JIVA single replica deployment and check if it gets scheduled immediately and the application is available' Pass $pipeline_id "$current_time" $commit_id $gittoken

if [ "$rc_val" != "0" ]; then
exit 1;
fi
}

if [ "$1" == "node" ];then
  node $2 $3 $4 $5
else
  pod
fi
