The test cases in the Cleanup stage are as follows:

| S.No | TID  | Test case description          |
| ---- | ---- | -------------------------------|
| 1    | ISTV | Bring down the konvoy cluster. |